<?php 

session_start();

function loginForm() {
	echo '
	<div class="container fix">
		<div class="loginBox fix">
			<form action="" method="POST">
				<p>Please Enter Your Name To Continue</p>
				<input type="text" name="name" id="name">
				<input type="submit" name="enter" value="Enter">
			</form>
		</div>
	</div>
	';
}


if (isset($_POST['enter'])) {
	if (!empty($_POST['name'])) {
		$name = $_POST['name'];
		$_SESSION['name'] = $name;
	} else{
        echo '<span class="error">Please Type a Name</span>';
    }
}


if(isset($_GET['logout'])){ 

    session_destroy();
    header("Location: index.php"); //Redirect the user

}

if (isset($_POST['submit'])) {
	$fp = fopen("message.txt", "a");
	date_default_timezone_set("Asia/Dhaka");
	  fwrite($fp, "<span class='name'>".$_SESSION['name'].": </span><span class='message'>".$_POST['message']." </span><span class='time'>".date('h:i:sa')."</span><br>");
	fclose($fp);
}


if (isset($_POST['clear'])) {
	file_put_contents("message.txt", "");
	header("Location: index.php");
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Chat System</title>
	<link rel="stylesheet" href="style.css">
</head>
<body>
<?php
	if(!isset($_SESSION['name'])){
	    loginForm();
	}
	else{
?>

	<div class="container fix">
		<div class="chat_box fix">
		    <div id="menu">
		        <p class="welcome">Welcome <span class="user"><?php echo ucwords($_SESSION['name']); ?></span></p>
		        <p class="logout"><a id="exit" href="#">Log Out</a></p>
		        <div style="clear:both"></div>
		    </div>
			<div class="chat_history">

				<div id="chatbox">
				<?php
					if(file_exists("message.txt") && filesize("message.txt") > 0){
					    $handle = fopen("message.txt", "r");
					    $contents = fread($handle, filesize("message.txt"));
					    fclose($handle);
					     
					    echo $contents;
					}
				?>
				</div>
				
			</div>	
		</div>
		<div class="chat_input fix">
			<form action="" method="POST">
				<textarea name="message" id="message" cols="30" rows="10" placeholder="Type Your Message Here" ></textarea>
				<input type="submit" name="submit" value="Reply">
				<input type="submit" name="clear" value="Clear History" onclick="alert('Are you sure want to Clear History?');">
			</form>
		</div>
	</div>

<?php

}

 ?>
<script src="jquery-2.1.0.min.js"></script>
 <script>
$(document).ready(function(){
	//If user wants to end session
	$("#exit").click(function(){
		var exit = confirm("Are you sure you want to end the session?");
		if(exit==true){window.location = 'index.php?logout=true';}		
	});
});
 </script>
</body>
</html>